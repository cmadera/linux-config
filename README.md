# Linux config


## Getting started

It is a repository of my Linux configuration. 

I start my initial setup from an Ubuntu Mini installation then I install Cinnamon on the Desktop.

The basic installation has a small number of problems to be solved with extra scrpts

### [Desktop](docs/DESKTOP.md) Initial setup
### [Server](docs/SERVER.md) Initial setup
### [Chat](docs/ROCKETCHAT.md) setup
### [Monitor](docs/UptimeKuma.md) setup


## Code Server (Visual Code Server)
```
curl -fsSL https://code-server.dev/install.sh | sh
sudo systemctl enable --now code-server@$USER
vi ~/.config/code-server/config.yaml
sudo systemctl restart --now code-server@$USER
```

[Source](https://coder.com/docs/code-server/install#debian-ubuntu)

[Add HTTPs](https://coder.com/docs/code-server/guide#using-lets-encrypt-with-nginx)

## Debian flavors
### Basic Menu
```
wget https://gitlab.com/cmadera/linux-config/-/raw/main/src/menu.sh
sh menu.sh
```

## Alpine
### Basic Tools
```
apk add mc tmux inxi openssh

```
### Basic Dev Tools
```
apk add nodejs npm git

```
