#!/bin/bash
#Cinnamon Installer for Ubuntu Minimal 18.04 (works also on 20.04)

clear
echo "Cinnamon Installer for use with Ubuntu Minimal / Ubuntu Server"
sudo apt -y update
sudo apt -y install software-properties-common
sudo add-apt-repository ppa:embrosyn/cinnamon
sudo apt -y update
sudo apt -y dist-upgrade
sudo apt -y install xorg dkms
sudo apt -y install cinnamon network-manager upower acpi-support synaptic
sudo apt -y install lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings --no-install-recommends 
sudo apt -y install gnome-system-monitor
# Sources & Drivers GUI configuration
sudo apt -y install software-properties-gtk

#Sound Install
sudo apt -y install alsa pulseaudio

#Video Install
sudo apt -y install libavcodec-extra ffmpeg

#BlueTooth Manager Install
sudo apt -y install blueman

# Optionals
# sudo apt -y install virtualbox-guest-dkms virtualbox-guest-x11 virtualbox-guest-utils 
sudo apt -y install chromium-browser
sudo apt install -y curl git net-tools build-essential tmux screen neofetch

echo "alias get='sudo apt install -y'" >> .bash_aliases 
echo "alias df='df -h'" >> .bash_aliases 
echo "alias ls='ls -la'" >> .bash_aliases 
echo "alias upgrade='sudo apt update -y && sudo apt full-upgrade -y && sudo apt autoremove -y'" >> .bash_aliases 
echo "alias myip='curl ifconfig.me'" >> .bash_aliases 
echo "alias version='lsb_release -a'" >> .bash_aliases 
echo "alias share='python3 -m http.server 8888'" >> .bash_aliases 

# Source: https://quidsup.net/sh/cinnamon.sh
