#!/bin/bash

### Source: https://www.cloudsavvyit.com/14248/how-to-set-up-rocket-chat-in-ubuntu/

read -p "enter a domain please (debian.local or chat.mydomain.com):" DOMAIN
echo $DOMAIN

# Install SNAP
sudo apt install snapd
# for Debian
sudo snap install --edge core

# Install RocketChat
sudo snap install rocketchat-server
sudo snap set rocketchat-server https=disable
sudo snap set rocketchat-server caddy-url=http://${DOMAIN}
sudo snap set rocketchat-server caddy=enable
sudo snap run rocketchat-server.initcaddy
sudo systemctl restart snap.rocketchat-server.rocketchat-server.service
sudo systemctl restart snap.rocketchat-server.rocketchat-caddy.service

# Install SSL
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
sudo certbot certonly --standalone

sudo snap set rocketchat-server https=enable
sudo snap set rocketchat-server caddy-url=https://${DOMAIN}
sudo snap set rocketchat-server caddy=enable
sudo snap run rocketchat-server.initcaddy
sudo systemctl restart snap.rocketchat-server.rocketchat-server.service
sudo systemctl restart snap.rocketchat-server.rocketchat-caddy.service

#sudo cat `https://${DOMAIN} {  proxy / localhost:3000 {   websocket    transparent  }}` > /var/snap/rocketchat-server/current/Caddyfile 
