#!/bin/bash

echo "source: https://opensource.com/article/20/1/improve-bash-scripts"
if [ "$USER" != 'root' ]; then
     echo "Sorry, this script must be run as root and you are $USER"
     exit 1
fi

read -p "enter a user please:" USERNAME
echo $USERNAME
read -p "enter a password please:" PASSWORD
echo $PASSWORD

##########################
echo "Setup UPGRADE"
apt update -y && apt upgrade -y
apt install sudo

##########################
echo "Create user"
read -p "Add new User? [Y/n]: " answuser
 if [ "$answuser" == 'y' ]; then
  useradd -m -p $(openssl passwd -1 ${PASSWORD}) -s /bin/bash -G sudo ${USERNAME}
  echo "${USERNAME} ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
 fi

##########################
echo "Setup SSH Config"
read -p "Apply ssh security? [Y/n]: " answsec
 if [ "$answsec" == 'y' ]; then
    ### SSH Config
    cp /etc/ssh/sshd_config /etc/ssh/sshd_config.bkp
    sed '/MaxAuthTries/d' /etc/ssh/sshd_config.bkp > /etc/ssh/sshd_config
    echo "MaxAuthTries 3" >> /etc/ssh/sshd_config
    grep "^MaxAuthTries" /etc/ssh/sshd_config

    cp /etc/ssh/sshd_config /etc/ssh/sshd_config.bkp
    sed '/PermitRootLogin/d' /etc/ssh/sshd_config.bkp > /etc/ssh/sshd_config
    echo "PermitRootLogin no" >> /etc/ssh/sshd_config
    grep "^PermitRootLogin" /etc/ssh/sshd_config

    /etc/init.d/ssh restart
 fi

##########################
echo "Install Basics"
apt install -y curl git net-tools build-essential tmux screen
apt install -y neofetch
apt install -y cockpit

##########################
echo "Setup .bash_aliases"
if [ "$answuser" == 'y' ]; then
    if [ -f /home/${USERNAME}/.bash_aliases ]; then
        rm /home/${USERNAME}/.bash_aliases
    fi

    echo "alias get='sudo apt install -y'" >> /home/${USERNAME}/.bash_aliases 
    echo "alias df='df -h'" >> /home/${USERNAME}/.bash_aliases 
    echo "alias ls='ls -la'" >> /home/${USERNAME}/.bash_aliases 
    echo "alias upgrade='sudo apt update -y && sudo apt full-upgrade -y && sudo apt autoremove -y'" >> /home/${USERNAME}/.bash_aliases 
    echo "alias cdwww='cd /var/www/html'" >> /home/${USERNAME}/.bash_aliases 
    echo "alias myip='curl ifconfig.me'" >> /home/${USERNAME}/.bash_aliases 
    echo "alias version='lsb_release -a'" >> /home/${USERNAME}/.bash_aliases 
    echo "alias share='python3 -m http.server 8888'" >> /home/${USERNAME}/.bash_aliases 

    sudo chown ${USERNAME}:${USERNAME} /home/${USERNAME}/.bash_aliases
fi
