#!/bin/bash

app_menu() {
    answer=$(
        whiptail --title 'App selection'         \
           --checklist 'What do you want to install?'  \
           --fb 20 50 10                           \
           tmux   'Multiple Terminal'  on \
           tmate  'Team Terminal'  off \
           inxi   'Hardware info'        on \
           glances 'Linux monitoring'     on \
           mc     'Midnight Commander'     on \
           notepadqq 'Notepad++'        off \
           git    'Git client'            off \
           net-tools 'Basic Net tools'  on \
           build-essential 'Build Essential' off \
           cockpit 'Remote administration' off  3>&1 1>&2 2>&3     )
    [ $? -ne 0 ] && return
    
    whiptail                                          \
        --title 'Wait'                           \
        --infobox "\nWe are going to install: \n'$answer' \n\nInstallation will takes a while..."  \
        0 0
    # Instead running all together as below
    # sudo apt install -y $answer
    # Is safer run one by one. Notepadqq is not always in the repository
    echo $answer | xargs -n 1 sudo apt-get install -y

}

add_alias(){
    echo "alias get='sudo apt install -y'" > .bash_aliases 
    echo "alias df='df -h'" >> .bash_aliases 
    echo "alias ls='ls -la'" >> .bash_aliases 
    echo "alias upgrade='sudo apt update -y && sudo apt full-upgrade -y && sudo apt autoremove -y'" >> .bash_aliases 
    echo "alias myip='curl ifconfig.me'" >> .bash_aliases 
    echo "alias version='lsb_release -a'" >> .bash_aliases 
    echo "alias share='python3 -m http.server 8888'" >> .bash_aliases 
    echo "alias listen='sudo lsof -i -P -n | grep LISTEN'" >> .bash_aliases
    echo "alias connected='sudo lsof -i -P -n | grep ESTABLISHED'" >> .bash_aliases
    source ~/.bashrc
    echo "Alias were added to your bash commands, check below:"
    cat .bash_aliases
    read something
}

node_install() {
    sudo apt-get update && sudo apt-get install -y ca-certificates curl gnupg
    curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
    NODE_MAJOR=20
    echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | sudo tee /etc/apt/sources.list.d/nodesource.list
    sudo apt-get update && sudo apt-get install nodejs -y
}

chrome_install() {
    # Chrome
    wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
    sudo dpkg -i google-chrome-stable_current_amd64.deb
    read something
}

docker_install() {
    if [  -n "$(uname -a | grep Ubuntu)" ]; then
        echo "Installing Docker on Ubuntu...."
        sudo apt-get remove docker docker-engine docker.io containerd runc
        sudo apt-get install -y ca-certificates gnupg lsb-release
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
        echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
        sudo apt-get update
        sudo apt-get install -y docker-ce docker-ce-cli containerd.io
    else
        echo "To be tested"
        curl -fsSL https://get.docker.com -o get-docker.sh
        sh ./get-docker.sh
        apt install docker-compose -y
        sudo chmod 666 /var/run/docker.sock
    fi

    sudo docker run hello-world
    read something
}

install_NotInstalled() {
    notInstalled=`which $1`
    echo $notInstalled
    if [ -z "$notInstalled" ] 
    then
        echo "Installing $1..." 
        sudo apt install $1
        echo "$1 Installed"
    fi
}

change_hostname() {
    #https://stackoverflow.com/questions/17752204/is-there-a-way-to-force-a-shell-script-to-run-under-bash-instead-of-sh
    if [ ! "$BASH_VERSION" ] ; then
        echo "Please do not use sh to run this script ($0), just execute it directly" 1>&2
        exit 1
    fi

    if [  -n "$(uname -a | grep Ubuntu)" ]; then

        newhost=$( whiptail --stdout --title 'Change hostname' --inputbox 'Please, type the new hostname:' 0 0 )

        echo $newhost > hostname
        sudo mv hostname /etc/hostname

        sed "s|$HOSTNAME|$newhost|g" /etc/hosts > hosts
        sudo mv hosts /etc/hosts

        echo "Hostname:"
        cat hostname

        echo "Hosts"
        cat hosts

        echo ''
        echo 'Please reboot the computer to apply the changes'
        read something
    
    else
        echo "This script is not ready for this OS"
    fi
}

echo "Checking for mandatory applications"
sudo apt update
install_NotInstalled sudo
install_NotInstalled curl
install_NotInstalled whiptail

while : ; do

    # Show the options
    answer=$(
      whiptail --title "Installation manager V1.1"  \
             --menu "Please select what do you need to install:" \
            --fb 17 50 7                  \
            "1" "Install essentials" \
            "2" "Install NodeJs"     \
            "3" "Install Docker"     \
            "4" "Install Chrome"     \
            "5" "Add basic alias"   \
            "6" "Change hostname"    \
            "0" "Exit"      3>&1 1>&2 2>&3     )

    # By Exit or ESC, then exit...
    [ $? -ne 0 ] && break

    # According by the option picked: 
    case "$answer" in
         1) app_menu ;;
         2) node_install ;;
         3) docker_install ;;
         4) chrome_install ;;
         5) add_alias ;;
         6) change_hostname ;;
         0) break ;;
    esac

done

echo 'Thanks for using the Installation Manager!' 

