# Desktop Initial setup

After install Ubuntu Mini run the following script:


```
wget https://gitlab.com/cmadera/linux-config/-/raw/main/src/desktop-setup.sh
sh desktop-setup.sh
```

## Issues

### WiFi Network Applet not conected

1) Set /etc/NetworkManager/NetworkManager.conf back to:

```
managed=false
```

2) Set /etc/network/interfaces back to:
```
auto lo
iface lo inet loopback
```

3) Change /etc/netplan/01-netcfg.yaml:

Rename 01-netcfg.yaml to 01-netcfg.yaml.HOLD for backup purposes.
```
sudo mv /etc/netplan/01-netcfg.yaml /etc/netplan/01-netcfg.yaml.HOLD
```

4) Create a new file called /etc/netplan/config.yaml...
```
sudo nano /etc/netplan/config.yaml # create/edit a new file
```

with the following content:
```
network:
  version: 2
  renderer: NetworkManager
```

5) Generate new config files
```
sudo netplan --debug generate
sudo netplan apply # apply new configuration
sudo reboot # reboot the computer
```

6) Use the standard NetworkManager menu to configure your "Wired Connection".

Verify Internet operation.

Source: https://askubuntu.com/questions/1135755/network-manager-applet-shows-not-connected-and-one-unmanaged-wired-connection 

### Chrome missing Icon 

After install Chrome, the regular Icon do not appear. To solve the problem install fonts-liberation
The tip is on the Chrome install process that say: "Package fonts-liberation is not installed."

```
sudo apt install fonts-liberation
```
