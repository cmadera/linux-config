
# Uptime Kuma Setup

Uptime Kuma is a fancy monitor for Websites and Internat networks

### First use Menu to install Node and Git

```
wget https://gitlab.com/cmadera/linux-config/-/raw/main/src/menu.sh
sh menu.sh 
```

### Then install npm and pm2
```
sudo npm install npm -g
sudo npm install pm2 -g
```

### Then clone the repository and install
```
git clone https://github.com/louislam/uptime-kuma.git
cd uptime-kuma/
npm run setup
```

### Test with
```
node server/server.js
```

### Is everything is ok, enable 80 port
```
sudo apt-get install -y libcap2-bin
sudo setcap cap_net_bind_service=+ep `readlink -f \`which node\``
```

### And run in production with
```
pm2 start server/server.js --name uptime-kuma -- --port=80 --host=0.0.0.0
```

[Here is working](https://mon.seeu.me)

### Add HTTPs
```
sudo certbot --non-interactive --redirect --agree-tos --nginx -d mon.seeu.me -m me@seeu.me
```